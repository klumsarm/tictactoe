import random
class Model:

    def __init__(self):							# Constructure for buid object
        self.table = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]	# List size 3x3 for storage X & O from input

    def get_table(self):
        return self.table
    
    def set_table(self,x,y,symbol):
        self.table[x][y] = symbol 
        
class Controller:

    def __init__(self):
        self.m = Model()
        self.v = Viewer(self.m)
        self.v.show_table()
        turnNumber = random.randint(1,10)							# Keep value of turn number by start at 1
        while(True):									# Infinite loop for checking
            self.turn(turnNumber)
            if(self.checkWin()):								# Call method check win
                if turnNumber % 2 == 0:							# Check who's turn is win X or O
                    print("X Win")								# Tell that X win
                else :
                    print("O Win")								# Tell that O win
                exit()									# Have a winner exit and close program
            self.checkDraw()
            turnNumber += 1

    def convert(self,n):
        x = int(float(n)//3.5)
        y = int(n)%3
        point = [x,y-1]
        return point
        
    def add(self,n,turnCount):        					                # Method that put X in table with specific loaction row and collum
        point = self.convert(n)   
        if turnCount % 2 == 0:
            self.m.set_table(point[0],point[1],'X')					# put X in specific row and collum recieved value from argument x & y
        else :
            self.m.set_table(point[0],point[1],'O')					# put O in specific row and collum recieved value from argument x & y
        self.v.show_table()						                        # Then after put O in table then show what it looks like

    def checkerror(self,n):						                # Check if input are only 0, 1, 2 if not then re-input
        if(len(n) != 1 or ord(n) < 49 or ord(n) > 57 ):
            print("\nERROR!!! : Try Again\n")
            self.v.show_table()
            return True

        point = self.convert(n)
        
        if(self.m.get_table()[point[0]][point[1]] != ' ' ):
            print("\nERROR!!! : Try Again\n")
            self.v.show_table()
            return True
        else:
            return False

    def checkWin(self):									# Check the current player turn win or not
        for x in range(3):								
            if(self.m.get_table()[x][0] == self.m.get_table()[x][1] == self.m.get_table()[x][2] != " "): 	# Check if player win by one in three row 
                return True							 	# If true return current player win
            elif(self.m.get_table()[0][x] == self.m.get_table()[1][x] == self.m.get_table()[2][x] != " "):	# Check if player win by one in three collum
                return True							 	# If true return current player win
            elif(self.m.get_table()[0][0] == self.m.get_table()[1][1] == self.m.get_table()[2][2] != " "):	# Check if player win by |\| this pattern
                return True							 	# If true return current player win
            elif(self.m.get_table()[2][0] == self.m.get_table()[1][1] == self.m.get_table()[0][2] != " "):	# Check if player win by |/| this pattern
                return True							 	# If true return cuurent player win 
        else:										# If none of this is true
            return False								# Return false nobody win keep playing or draw
    def checkDraw(self):								# Check draw
        blank = 0
        for x in range(3):
            for y in range(3):								# Check if any value in list = " " then keep playing 
                if self.m.get_table()[x][y] == " ":						# If not then blank = 0
                    blank +=1
        if blank == 0:									# When blank = 0 then it means draws
            print("Draw")								# Let the player know that this turn is draw
            exit()		

    def turn(self,turnCounter):								# Tell who's turn O or X and count turn
        if turnCounter % 2 == 0:								# If true it mean X turn if not Y turn 
            print("X turn")
        else :										
            print("O turn")
        
        n = input("Choose Position : ")
    
        if(self.checkerror(n)):								
            self.turn(turnCounter)
        else:										# If not error then add X or O from x,y input
            self.add(n,turnCounter)
        

class Viewer:
    def __init__(self,m):
        self.m = m
    def show_table(self):						# Draw table and show what is inside in each collum and row
        num = 1
        for i in range(0,len(self.m.get_table())):				
            for j in range(0,len(self.m.get_table()[i])):
                print("|"+self.m.get_table()[i][j] ,end= "|")
            print("   " ,end="")
            print("|"+str(num)+"|"+"|"+str(num+1)+"|"+"|"+str(num+2)+"|" ,end= "")
            num += 3
            print()
            
def main():
    ox = Controller()

    
    
main()
 
